# The World's Coffee
### Setting Up a Dev Environment

1. Be sure you have [Node](https://nodejs.org/en/download/), [Heroku](https://devcenter.heroku.com/articles/heroku-cli), and [MongoDB Community Edition](https://docs.mongodb.com/manual/installation/#mongodb-community-edition) installed on your computer. Follow the links to install.

2. Clone coffee-api, coffee-admin, and coffee-client. 
3. Install their dependencies by going to all 3 directories and running `npm install`.
4. Begin the MongoDB process by running `mongod` or `sudo mongod`.
5. Open a new terminal, navigate into the coffee-api directory, and run `node server.js`. This should start the server on port 3000, accessible at http://localhost:3000.
6. You can now run coffee-client or coffee-admin by navigating to their directories and running `npm run start`. You'll probably have to enter a second time, allowing the app to be run a port other than 3000. The infrastructure provided by *Create React App* will spin up a dev server for the app, as well as automatically update it each time a file is saved.


### Git Explanation
The World's Coffee comprises of 3 git *repos*, 
* [coffee-client](https://gitlab.com/nickroberts404/coffee-client)
* [coffee-api](https://gitlab.com/nickroberts404/coffee-api)
* [coffee-admin](https://gitlab.com/nickroberts404/coffee-admin)

Each of theses repos has 2 *branches*; **master** and **heroku-b** (short for **heroku-b**ranch).

Theses two only differ slightly, **heroku-b** having the code that will be pushed to the live heroku server, and **master** having the code that you use to test and check before merging it into **heroku-b**. 
The most notable differences between the two are
* **heroku-b** branches have the live stripe keys, **master** branches should have the test stripe keys.
* **heroku-b** branches have the real api-endpoint (https://gentle-wave-57193.herokuapp.com right now), **master** branches should have the endpoint of a server running locallly. (This is optional, but it helps keep testing data and real data seperate.)

Each repo has one version on your computer (local), and 2 remotes (copies of the repo on some other server). The main remote is origin (aka gitlab), the other is heroku. 

When making changes, only the repo pushed to heroku will make any difference to the live site. You should also push to origin though, to keep everything synced. 

If there is more than one person working on a repo, it is important to `git pull` often, keeping your local repos synced.

#### Setting up Your Local Repo

There are many ways to handle all of this git stuff, I'll describe one.
1. Clone the repo(s) from gitlab. Gitlab will provide a URL, and you just type `git clone <url>`. You now have a local repo and a remote repo, gitlab is the remote repo and it is called *origin*. You'll push and pull between the two to keep them synced (that way others can work on it too). 
2. Now you need to add heroku as a remote, so you can push your repo to it as well. While in your git repo, run the command `heroku git:remote -a <heroku-app-name>`. (If this doesn't work, make sure you've installed the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli))

### Pushing to Heroku

After making changes in **master**, you need a way to move them to **heroku-b** without erasing all of the things that make **heroku-b** special. We do this by using a feature of git called **merging**.  

After making changes in **master**, we switch to our **heroku-b** branch (`git checkout heroku-b`) and run the command `git merge master`. This takes the changes in the **master** branch and applies them to **heroku-b**, leaving the new updated **heroku-b** branch in its place.

Once the **heroku-b** branch is ready to be pushed live, run the command `git push -f heroku heroku-b:master`. This pushes the **heroku-b** branch to the heroku origin git server. The `:**master**` part is a way of saying push the **heroku-b** branch, but on the remote heroku repo, make this the **master** branch (because that is the only branch heroku serves from).

This is also the time that you should push to origin, probably just by typing `git push`, perhaps `git push origin` if that doesn't work. Usually if pushes don't work, it's because you need to `git pull`.

### Current Services
The World's Coffee is currently using **Stripe** to process payments, **Mapbox** to display the map, **Heroku** to host the site, and a Heroku add-on called **mLab** as the database. 

#### Stripe
Stripe's role begins when a user checks out. The library [react-stripe-checkout](https://www.npmjs.com/package/react-stripe-checkout) is used on the [Cart](https://gitlab.com/nickroberts404/coffee-client/blob/**master**/src/Pages/Cart.js) page to provide a checkout experience. After calculating how much to charge the customer, we pass this data to the component which prompts the user for their checkout info. 

Once this is done, the Stripe component generates a token for the order. 
The token is sent to the coffee-api server and gets routed to the [create function](https://gitlab.com/nickroberts404/coffee-api/blob/**master**/routes/order.js#L14) for orders. This is where we interact with the [stripe](https://www.npmjs.com/package/stripe) library and actually create the charge.

#### Mapbox
Mapbox is used to provide the actual images for the map. It's also what we use on the admin site to provide a map snapshot for roaster location, as well as to convert the lat lng combo into a city and state (this process is called reverse geocoding). 

Add your own mapbox key in the repo's config. Mapbox also offers extensive customization options on their website. If you want to change the map style, you can change [`mapboxStyle`](https://gitlab.com/nickroberts404/coffee-client/blob/**master**/src/config.js#L3) in the config.

The map's behavior is defined in  [mapboxUtil.js](https://gitlab.com/nickroberts404/coffee-client/blob/**master**/src/mapboxUtil.js) and [FullMap.js](https://gitlab.com/nickroberts404/coffee-client/blob/**master**/src/Components/FullMap/FullMap.js). This is where the code is for creating the map and displaying the markers, as well as all the complicated stuff like making a certain marker darker than the others when hovered over. 

#### Heroku
Most heroku stuff has already been written above. The World's Coffee is currently using the free plan of Heroku, which is why sometimes it takes a long time to load. To save money, each dyno (aka app/server/instance) falls asleep after 30 minutes of not being used, and takes around 5 seconds to turn back on. 

If the client is taking 5 seconds to turn back on, then making a request to the API which takes 5 seconds to turn back on, it could take amost 10 seconds for data to actually reach the site, resulting in some ugly /empty pages. This can be resolved by upgrading your Heroku plan.



