import React, { Component } from 'react';
import {IndexLink, Link} from 'react-router';
import NavSearchBar from '../SearchBar/NavSearchBar';

// <svg height="32px" id="Layer_1" style="enable-background:new 0 0 32 32;" version="1.1" viewBox="0 0 32 32" width="32px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M4,10h24c1.104,0,2-0.896,2-2s-0.896-2-2-2H4C2.896,6,2,6.896,2,8S2.896,10,4,10z M28,14H4c-1.104,0-2,0.896-2,2  s0.896,2,2,2h24c1.104,0,2-0.896,2-2S29.104,14,28,14z M28,22H4c-1.104,0-2,0.896-2,2s0.896,2,2,2h24c1.104,0,2-0.896,2-2  S29.104,22,28,22z"/></svg>
export default class Nav extends Component {

	constructor(props) {
		super(props);
		this.state = {active: false};
	}

	render() {
		const { active } = this.state;
		return (
			<nav className="page-nav">
				<div className="inner-nav container">
					<Link to='/' className="logo-icon-container">
						<img src="/logo.png" alt="Logo Icon" className="logo-icon"/>
					</Link>
					<Link className='logo' to='/'>The Worlds Coffee</Link>
					<IndexLink className="nav-link main-nav-link" to="/" activeClassName="active-nav-link">Home</IndexLink>
					<Link className="nav-link main-nav-link" to="/about" activeClassName="active-nav-link">About</Link>
					<Link className="nav-link main-nav-link" to="/map" activeClassName="active-nav-link">Map</Link>
					<Link className="nav-link main-nav-link" to="/catalog" activeClassName="active-nav-link">Catalog</Link>
					<div className="flex-spacer"></div>
					<NavSearchBar />
					<Link className="nav-link" to="/cart" activeClassName="active-nav-link">Cart <span>({this.props.cartSize})</span></Link>
					<button className="menu-button" type="button" onClick={() => this.setState({active: !active})}>
						<img src="/menu-icon.svg" alt="menu icon"/>
					</button>
				</div>
				<div className={'responsive-nav ' + (active ? 'active' : '')}>
					<IndexLink className="nav-link" to="/" activeClassName="active-nav-link" onClick={() => this.setState({active: false})}>Home</IndexLink>
					<Link className="nav-link" to="/about" activeClassName="active-nav-link" onClick={() => this.setState({active: false})}>About</Link>
					<Link className="nav-link" to="/map" activeClassName="active-nav-link" onClick={() => this.setState({active: false})}>Map</Link>
					<Link className="nav-link" to="/catalog" activeClassName="active-nav-link" onClick={() => this.setState({active: false})}>Catalog</Link>
					<NavSearchBar className={'responsive-nav-searchbar'}/>
				</div>
			</nav>
		)
	}
}
