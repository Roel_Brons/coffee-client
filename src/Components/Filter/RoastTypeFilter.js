import React, { Component } from 'react';

export default class RoastTypeFilter extends Component {

	updateQuery(roast) {
		const { updateQuery } = this.props;
		updateQuery({roast});
	}

	render() {
		const { query, properties } = this.props;
		return (
			<div className="filter-section">
				<div className="filter-section-title">Roast Type</div>
				<div className="filter-section-options">
					{Object.keys(properties).map(i => {
						return <div
							key={i}
							className={"filter-option " + (query.roast && query.roast === i ? 'filter-option-active' : '')}
							onClick={() => this.updateQuery(i)}>{i} <span>({properties[i]})</span></div>
					})}
				</div>
			</div>
		)
	}

}