import React, { Component } from 'react';
import BeanFilter from './BeanFilter';
import RoasterFilter from './RoasterFilter';

export default class Filter extends Component {

	constructor(props) {
		super(props);
		this.state = {active: false};
	}

	getFilterComponent() {
		const { beans, roasters, query, updateQuery } = this.props;
		if ( (query.subject || 'beans')  === 'beans' ) {
			return <BeanFilter data={beans} query={query} updateQuery={updateQuery} active={this.state.active} />
		} else {
			return <RoasterFilter data={roasters} query={query} updateQuery={updateQuery} active={this.state.active} />
		}
	}

	render() {
		const { updateQuery, query } = this.props;
		const subjects = [{label: 'Beans', value: 'beans'}, {label: 'Roasters', value: 'roasters'}];
		return (
			<div className="catalog-filter">
				<div className="top-filter-panel">
					<div className="filter-subject">
						{subjects.map(i => (
							<div className="filter-subject-item">
								<input
									type="radio"
									className='filter-subject-radio'
									name="subject"
									value={i.value}
									id={"subject-"+i.value}
									checked={(query.subject || 'beans') === i.value}
									onChange={ () => updateQuery({subject: i.value})}
									/>		
								<label htmlFor={"subject-"+i.value}>{i.label}</label>
							</div>
						))}
					</div>
					<div className="filter-toggle" onClick={() => this.setState({active: !this.state.active})}>Filters</div>
				</div>
				
				{this.getFilterComponent()}
			</div>
		)
	}

}