import React, { Component } from 'react';
import RoastTypeFilter from './RoastTypeFilter';
import PriceFilter from './PriceFilter';
import LocationFilter from './LocationFilter';

export default class BeanFilter extends Component {

	countProperties(arr=[]) {
		return arr.reduce((o, i) => {
			if (!o.roast[i.roast]) o.roast[i.roast] = 1;
			else o.roast[i.roast]++;

			if(i.price <= 5) o.price[5]++;
			else if	(i.price <= 10) o.price[10]++;
			else if	(i.price <= 15) o.price[15]++;
			else if	(i.price <= 20) o.price[20]++;
			else o.price[21]++;

			if (o.location[i.roaster.location.city] === undefined ) o.location[i.roaster.location.city] = 1;
			else o.location[i.roaster.location.city]++;
			
			return o;
		}, {
			roast: {
				light: 0,
				medium: 0,
				dark: 0,
				other: 0
			},
			price: {
				5: 0,
				10: 0,
				15: 0,
				20: 0,
				21: 0,
			},
			location: {},
		});
	}

	render() {
		const { data, query, updateQuery, active } = this.props;
		const properties = this.countProperties(data);
		return (
			<div className={'filter-panel bean-filter'+(active ? ' filter-panel-active' : '')}>
				<RoastTypeFilter query={query} updateQuery={updateQuery} properties={properties.roast} />
				<PriceFilter query={query} updateQuery={updateQuery} properties={properties.price} />
				<LocationFilter query={query} updateQuery={updateQuery} properties={properties.location} />
			</div>
		)
	}

}