import React, { Component } from 'react';
import LocationFilter from './LocationFilter';

export default class BeanFilter extends Component {

	countProperties(arr=[]) {
		return arr.reduce((o, i) => {

			if (o.location[i.location.city] === undefined ) o.location[i.location.city] = 1;
			else o.location[i.location.city]++;
			
			return o;
		}, {
			location: {},
		});
	}

	render() {
		const { data, query, updateQuery, active } = this.props;
		const properties = this.countProperties(data);
		return (
			<div className={'filter-panel roaster-filter'+(active ? ' filter-panel-active' : '')}>
				<LocationFilter query={query} updateQuery={updateQuery} properties={properties.location} />
			</div>
		)
	}

}