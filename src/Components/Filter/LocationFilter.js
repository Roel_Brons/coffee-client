import React, { Component } from 'react';

export default class LocationFilter extends Component {

	updateQuery(location) {
		const { updateQuery } = this.props;
		updateQuery({location});
	}

	render() {
		const { query, properties } = this.props;
		return (
			<div className="filter-section">
				<div className="filter-section-title">Location</div>
				<div className="filter-section-options">
					{Object.keys(properties).map(i => {
						return <div
							key={i}
							className={"filter-option "+(query.location === i ? 'filter-option-active' : '')} 
							onClick={() => this.updateQuery(i)}>{i} <span>({properties[i]})</span></div>
					})}
				</div>
			</div>
		)
	}

}