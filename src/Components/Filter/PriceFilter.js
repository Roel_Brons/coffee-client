import React, { Component } from 'react';

const priceOptions = [
	{ maxPrice: 5, property: 5, text: '< $5' },
	{ maxPrice: 10, minPrice: 5, property: 10, text: '$5 - $10' },
	{ maxPrice: 15, minPrice: 10, property: 15, text: '$10 - $15' },
	{ maxPrice: 20, minPrice: 15, property: 20, text: '$15 - $20' },
	{ minPrice: 20, property: 21, text: '> $20' },
]

export default class PriceFilter extends Component {

	updateQuery(priceMin, priceMax) {
		const { updateQuery } = this.props;
		if(priceMin === undefined) updateQuery({priceMax});
		else if(priceMax === undefined) updateQuery({priceMin});
		else updateQuery({priceMin, priceMax});
	}
	
	render() {
		const { query, properties } = this.props;
		return (
			<div className="filter-section">
				<div className="filter-section-title">Price</div>
				<div className="filter-section-options">
					{priceOptions.map(i => {
						return <div
							key={i.property}
							className={"filter-option " + ((query.priceMin == i.minPrice) && (query.priceMax == i.maxPrice) ? 'filter-option-active' : '')}
							onClick={() => this.updateQuery(i.minPrice, i.maxPrice)}>{i.text} <span>({properties[i.property]})</span></div>
					})}
				</div>
			</div>
		)
	}

}