import React, { Component } from 'react';
import {hashHistory} from 'react-router';
import { initializeMap, geoJSON } from '../../mapboxUtil';

const circleRadius = {stops: [[8, 3], [11, 7], [16, 15]]};

export default class FullMap extends Component {

	constructor(props) {
		super(props);
		this.state = {map: null};
	}

	componentDidMount() {
		this._isMounted = true;
		const { lat, lng, zoom } = this.props.query;
		const map = initializeMap('map', lat, lng, zoom)
		this.setState({map});
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	componentDidUpdate() {
		this.setupMap();
	}

	addSourceAndLayers(map) {
		const { roasters } = this.props;
		map.addSource('points', {
			type: 'geojson',
			data: geoJSON(roasters, i => [i.location.lng, i.location.lat]),
		});
		map.addLayer({
			id: 'points',
			type: 'circle',
			source: 'points',
			paint: {
				'circle-radius': circleRadius,
				'circle-color': '#1eaedb',
			}
		})
		map.addLayer({
			id: 'highlight',
			type: 'circle',
			source: 'points',
			paint: {
				'circle-radius': circleRadius,
				'circle-color': '#1eaedb'
			},
			filter: ['==', 'id', ""]
		});
	}

	addDataListener(map) {
		let dataCount = 0;
		let dataListenLimit = 4;
		const listener = (d) => {
			this.updateVisibleFeatures.call(this);
			if (dataCount++ > dataListenLimit) map.off('data', listener);
		}
		map.on('data', listener);
	}

	addMouseListener(map) {
		const { updateHighlighted, updateActive } = this.props;
		map.on('mousemove', function(e) {
			var features = map.queryRenderedFeatures(e.point, {layers: ['points'] });
			// Change the cursor style as a UI indicator.
			map.getCanvas().style.cursor = features.length ? 'pointer' : '';
			if (features.length <= 0) {
				updateHighlighted(null);
				map.setFilter("highlight", ['==', 'id', ""]);
				map.setPaintProperty('points', 'circle-opacity', 1);
				return false;
			}
			var feature = features[0];
			map.setFilter("highlight", ["==", "id", feature.properties.id]);
			map.setPaintProperty('points', 'circle-opacity', 0.5);
			updateHighlighted(feature.properties.id);
		});
		map.on('click', function(e) {
			var features = map.queryRenderedFeatures(e.point, {layers: ['points'] });
			var feature = features[0];
			if (feature) updateActive(feature.properties.id);
		});
		map.on('moveend', () => {
			this.updateVisibleFeatures();
			this.updateQuery();
		});
	}

	highlightMarker(map) {
		const { highlighted } = this.props;
		if(highlighted) {
			map.setFilter("highlight", ["==", "id", highlighted]);
			map.setPaintProperty('points', 'circle-opacity', 0.2);
		} else {
			map.setFilter("highlight", ['==', 'id', ""]);
			map.setPaintProperty('points', 'circle-opacity', 1);
		}
	}

	setupMap() {
		const { map } = this.state;
		const { roasters, highlighted, updateHighlighted } = this.props;
		if(!map || roasters.length <= 0) return false;
		map.on('load', () => {
			this.addSourceAndLayers(map);
			this.addDataListener(map);
			this.addMouseListener(map);
		});
		if (map._loaded) {
			this.highlightMarker(map);
		}

	}

	updateVisibleFeatures() {
		if(!this._isMounted) return false;
		var features = this.state.map.queryRenderedFeatures({layers:['points']});
		if (features) {
			const uniqueFeatures = this.getUniqueFeatures(features, 'id');
			this.props.update(uniqueFeatures);
		}
	}

	updateQuery() {
		if(!this._isMounted) return false;
		const center = this.state.map.getCenter();
		const zoom = this.state.map.getZoom();
		hashHistory.push(`/map?lat=${center.lat}&lng=${center.lng}&zoom=${zoom}`);
	}

	getUniqueFeatures(array, comparatorProperty) {
	    var existingFeatureKeys = {};
	    // Because features come from tiled vector data, feature geometries may be split
	    // or duplicated across tile boundaries and, as a result, features may appear
	    // multiple times in query results.
	    var uniqueFeatures = array.filter(function(el) {
	        if (existingFeatureKeys[el.properties[comparatorProperty]]) {
	            return false;
	        } else {
	            existingFeatureKeys[el.properties[comparatorProperty]] = true;
	            return true;
	        }
	    });
	    return uniqueFeatures;
	}
	
	render() {
		return (
			<div id="map" className="map"></div>
		)
	}
}