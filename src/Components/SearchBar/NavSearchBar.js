import React, { Component } from 'react';
import {hashHistory} from 'react-router';

export default class SearchBar extends Component {

	constructor(props) {
		super(props);
		this.state = {searchTerm: ''};
	}

	onSearch() {
		const { searchTerm } = this.state;
		this.setState({searchTerm: ''});
		hashHistory.push('/catalog?search='+searchTerm);
	}
	
	render() {
		return (
			<div className={'nav-searchbar '+(this.props.className || '')}>
				<input
					type="text"
					placeholder="Search..."
					value={this.state.searchTerm}
					onChange={e => this.setState({searchTerm: e.target.value})}
					onKeyDown={e => e.keyCode === 13 ? this.onSearch() : null} />
				<button className="nav-search-button flex-center-full" type="button" onClick={this.onSearch.bind(this)}>
					<img src="/search.svg" alt="search button"/>
				</button>
			</div>
		)
	}
}