import React, { Component } from 'react';
import {IndexLink, Link} from 'react-router';
import { footerText } from '../../config.js';

const colorBlack = {
	color: 'black',
};
//Change to make- Link these to /privacy and /terms respectively
//They currently exist in ./Pages but do not render their page contents

export default class Footer extends Component {

	render() {
		return (
			<footer className="page-footer">
				<div className="container footer-container">
					<p>
						<Link to='/' className="logo-icon-container">
							<img src="/logo.png" alt="Logo Icon" className="logo-icon"/>
						</Link>
						<br />
					  {footerText}
						<br />
						<Link to="/about" style={colorBlack}>Privacy Policy</Link>
						<br />
						<Link to="/about" style={colorBlack}>Terms Of Service</Link>
					</p>
				</div>
			</footer>
		)
	}
}
