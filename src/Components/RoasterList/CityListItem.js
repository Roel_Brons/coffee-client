import React, { Component } from 'react';
import RoasterListItem from './RoasterListItem';

export default class CityListItem extends Component {

	render() {
		const { highlighted, updateHighlighted, active, updateActive } = this.props;
		const { city, roasters } = this.props.data;
		return (
			<div className="city-list-item">
				<div className="roaster-list-items">
					{roasters.map(i => <RoasterListItem
						key={i._id}
						data={i}
						active={active}
						updateActive={updateActive}
						highlighted={highlighted}
						updateHighlighted={updateHighlighted} />)}
				</div>
			</div>
		)
	}
}