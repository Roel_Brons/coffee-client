import React, { Component } from 'react';
import CityListItem from './CityListItem';

export default class RoasterList extends Component {

	categorizeRoasters(roasters) {
		if(roasters.length <= 0) return [];
		let cityDict = this.props.roasters.reduce((d, r) => {
			let temp = {};
			let city = r.location.city;
			if(d[city] != undefined) {
				temp[city] = d[city].concat([r]);
			} else {
				temp[city] = [r];
			}
			return Object.assign({}, d, temp);
		}, []);
		let cityArr = [];
		Object.keys(cityDict).forEach( i => {
			cityArr = cityArr.concat([{city: i, roasters: cityDict[i]}]);
		})
		return cityArr;
	}

	render() {
		const { highlighted, updateHighlighted, active, updateActive } = this.props;
		let categorizedRoasters = this.categorizeRoasters(this.props.roasters);
		return (
			<div className="roaster-list">
				{categorizedRoasters.map(c => <CityListItem
					key={c.city}
					data={c}
					active={active}
					updateActive={updateActive}
					highlighted={highlighted}
					updateHighlighted={updateHighlighted} />)}
			</div>
		)
	}
}