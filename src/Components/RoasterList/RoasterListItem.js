import React, { Component } from 'react';
import { Link } from 'react-router';

export default class RoasterListItem extends Component {

	render() {
		const { highlighted, updateHighlighted, active, updateActive } = this.props;
		const { _id, name, photoUrl, location, shortDescription, coffees } = this.props.data;
		let extraClass = highlighted === _id ? ' roaster-list-item-highlighted' : '';
		extraClass += active === _id ? ' roaster-list-item-active' : '';
		return (
			<div className={"roaster-list-item"+ extraClass}
				onMouseOver={() => updateHighlighted(_id)}
				onMouseOut={() => updateHighlighted(null)} >
				<div className="roaster-list-item-top" onClick={() => updateActive((active === _id ? null : _id))}>
					<div className="roaster-list-img-container">
						<img src={photoUrl} alt={name} />
					</div>
					<div className="flex-spacer roaster-top-info">
						<div className="roaster-list-item-title">{name}</div>
						<div className="roaster-list-item-location">{location.city}, {location.state}</div>
					</div>
				</div>
				<div className="roaster-list-item-content">
					<div className="roaster-list-item-description">{shortDescription}</div>
					<div className="roaster-bottom-info">
						<div className="roaster-list-item-count">{coffees.length} beans</div>
						<Link className="roaster-list-item-link" to={`/roaster/${_id}`}>See More</Link>
					</div>
				</div>
			</div>
			
		)
	}
}