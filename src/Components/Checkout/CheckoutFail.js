import React, { Component } from 'react';
import { Link } from 'react-router';

export default class CheckoutFail extends Component {

	render() {
		return (
			<div className="order-status-page page container flex-center-full">
				<div className="sorry-message flex-center-column">
					<span className="main-sorry">Oops!</span>
					<span>An error has occured while processing your order.</span>
					<span>We're sorry about this, <Link to='cart'>try again!</Link></span>
				</div>
			</div>
		)
	}

}