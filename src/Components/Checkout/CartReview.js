import React, { Component } from 'react';
import ItemReview from './ItemReview'
export default class CartReview extends Component {

	render() {
		const { cart, removeFromCart, quantities, updateQuantities } = this.props;
		return (
			<div className="cart-review">
				<div className="cart-review-title">Review Cart</div>
				<table className="cart-table">
					<tbody>
						{cart.map(i => <ItemReview 
							data={i}
							key={i._id}
							quantity={quantities[i._id] || 1}
							removeFromCart={removeFromCart}
							updateQuantities={updateQuantities} />)}
					</tbody>
				</table>
			</div>
		)
	}

}