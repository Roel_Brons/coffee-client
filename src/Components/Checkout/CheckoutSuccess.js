import React, { Component } from 'react';
import OrderReview from './OrderReview';

export default class CheckoutSuccess extends Component {

	constructor(props) {
		super(props);
		this.state = {cart: props.cart, quantities: props.quantities, rates: props.rates}
	}

	componentWillReceiveProps(nextProps) {
		this.setState({cart: nextProps.cart, quantities: nextProps.quantities})
	}

	componentWillMount() {
		this.props.emptyCart();
	}

	render() {
		const { cart, quantities, rates } = this.state; 
		
		return (
			<div className="order-status-page page container flex-center-full">
				<div className="thank-you-message flex-center-column">
					<span className="main-thank-you">Thank You!</span>
					<span>Your order has been processed and a confirmation will be emailed to you shortly.</span>
					<span className="signoff">Thank you for shopping with Worlds Coffee!</span>
				</div>
				<div className="cart-review">
					<div className="cart-review-title">Order Review</div>
					<table className="cart-table">
						<thead>
							<tr><th>Name</th><th>Price</th><th>Shipping</th><th>Quantity</th></tr>
						</thead>
						<tbody>
							{cart.map((i, j) => {
									
								return(<OrderReview 
									data={i} 
									rate={rates[j]}
									key={i._id}
								quantity={quantities[i._id] || 1} />)
							})}
						</tbody>
						<tfoot>
							<tr>
								<td>Total</td>
								<td colSpan="2"></td>
								<td>${this.props.total}</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			
		)
	}

}