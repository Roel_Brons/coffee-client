import React, { Component } from 'react';  
import { Link } from 'react-router'; 
import CheckoutSuccess from './CheckoutSuccess';
import CheckoutFail from './CheckoutFail';  
import { apiEndpoint, stripeKey } from '../../config';
import request from 'request';

export default class OrderConfirmation extends Component { 

	constructor(props) {
		super(props);


		var default_prices = [], default_rates = []; 
		for (var i = 0; i < props.order.items.length; i++) {
			for (var j = 0; j < props.order.items[i].rates.length; j++) {
				if (props.order.items[i].rates[j].service == 'First-Class Package/Mail Parcel') {
					default_prices.push(props.order.items[i].rates[j].amount); 
					default_rates.push(props.order.items[i].rates[j]); 
				} 
				if (props.order.items[i].rates.length == 1) {
					default_prices.push(props.order.items[i].rates[j].amount); 
					default_rates.push(props.order.items[i].rates[j]); 
				}
			}
		}

		this.state={
			cart: props.cart, 
			quantities: props.quantities, 
			order: props.order, 
			items: props.order.items, 
			shipping_prices: new Array(props.cart.length), 
			selected_prices: default_prices, 
			selected_rates: default_rates, 
			total_prices: [], 
			orderConfirmed: false, 
			orderFailes: false 
		} 


		this.getShippingPrice=this.getShippingPrice.bind(this); 
		this.getTotalPrice=this.getTotalPrice.bind(this); 
		this.getTotalItemPrice=this.getTotalItemPrice.bind(this); 
		this.placeOrder=this.placeOrder.bind(this); 

	}

	componentWillReceiveProps(nextProps) {
		this.setState({cart: nextProps.cart, quantities: nextProps.quantities, order: nextProps.order})
	}


	getTotalPrice(pr) {
	
		var prices = [], q; 
		for (var i = 0; i < this.state.cart.length; i++) {

			q=this.props.quantities[this.state.cart[i]._id] || 1; 
			prices.push(this.getTotalItemPrice(this.state.cart[i], i, q, true, pr))
		}

		function add(a, b) {

			return parseFloat(a) + parseFloat(b); 
		}
	
		var total_sum = prices.reduce(add, 0); 
		this.setState({ total: total_sum.toFixed(2) })

	}

	getTotalItemPrice(item, i, q, num, prices) {

		typeof prices == 'undefined' ? prices = parseFloat(this.state.selected_prices[i]) : prices = parseFloat(prices[i]); 
		var sum = prices + (item.price * q);

		if (typeof num == 'undefined') { 
			return `$${sum.toFixed(2)}`; 
		} else {
			return sum.toFixed(2); 
		}

	} 

	componentDidMount() {
		this.getTotalPrice(); 
	}

	getShippingPrice(rates, i) { 
	
		let options=[]; 
		for (var j=0; j < rates.length; j++) {
			
			options.push(<option key={j * Math.random()} data-index={j} value={rates[j].amount}>{`${rates[j].service} - $${rates[j].amount}`}</option>);
		} 

		return (
		<select data-index={i}
			value={this.state.selected_prices[i]} 
			onChange={(e)=> {
	
				var active_prices=this.state.selected_prices.slice(); 
				var active_rates=this.state.selected_rates.slice(); 
				
				active_prices[parseInt(e.target.dataset.index)]=e.target.value;  
				
				for (var j = 0; j < this.props.order.items[parseInt(e.target.dataset.index)].rates.length; j++) {
					if (this.props.order.items[parseInt(e.target.dataset.index)].rates[j].amount == e.target.value) {
						active_rates[parseInt(e.target.dataset.index)] = this.props.order.items[parseInt(e.target.dataset.index)].rates[j]; 
					}
				}
				
				this.setState({
					selected_prices: active_prices,  
					selected_rates: active_rates
				}); 

				this.getTotalPrice(active_prices); 
			}}>
			{options}
		</select>
		)
	} 

	placeOrder() {
		
		var order_data = this.props.order; 
		for (var i = 0; i < order_data.items.length; i++) {
			order_data.items[i].rates = this.state.selected_rates[i]; 
		}	
		order_data.charge = parseInt(this.state.total * 100); 
		
		// Send Order to API
		request({
			method: 'POST',
			uri: `${apiEndpoint}/order`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify(order_data),
		}, (err, res, body) => { 
		
			if(err || res.statusCode === 400) this.setState({orderFailed: true})
			else {
				
				var b = JSON.parse(body); 
				var mail_data = Object.assign({}, b, {total: this.state.total}); 
				
				request({
					method: 'POST', 
					uri: `${apiEndpoint}/mail`, 
					headers: {
						'Content-Type': 'application/json'
					}, 
					body: JSON.stringify(mail_data)
				})
				
				this.setState({orderConfirmed: true, order: JSON.parse(body)})
			}
		})

	}

	render() { 
		const { quantities, emptyCart, getTotalPrice } = this.props;
		const { orderConfirmed, orderFailed, cart, total, selected_prices } = this.state;
		if (orderConfirmed) return <CheckoutSuccess cart={cart} quantities={quantities} emptyCart={emptyCart} total={total} rates={selected_prices} /> 
		else if (orderFailed) return <CheckoutFail />
		return(
			<div className="order-status-page page container flex-center-full">
				<div className="flex-center-column"><h3>Confirm Your Order</h3></div> 
				<div className="cart-review">
					<table className="cart-table order-confirmation-desktop">
						<thead>
							<tr><th>Name</th><th>Quantity</th><th>Price</th><th>Shipping</th><th>Total with shipping</th></tr>
						</thead>
						<tbody>
							{this.props.cart.map((item, i)=> { 
								
								var q=this.props.quantities[item._id] || 1;
								
								return (
					
									<tr key={item._id}>
										<td><Link className="item-review-name" to={`/bean/${item._id}`}>{item.name}</Link><Link to={`/roaster/${item.roaster._id}`} className="item-review-roaster">{item.roaster.name}</Link></td>
										<td>{q}</td>
										<td>{`$${item.price.toFixed(2)} each`}</td>
										<td>{this.getShippingPrice(this.state.items[i].rates, i)}</td>
										<td>{this.getTotalItemPrice(item, i, q)}</td>
									</tr>
								)
							})}
						</tbody>
						<tfoot>
							<tr>
								<td>Total</td>
								<td>${this.state.total}</td>
								<td colSpan="3"><button onClick={this.placeOrder} className="checkout-button button-primary order-confirmation-button">Place order</button></td>
							</tr>
						</tfoot>
					</table>
					<div className="order-confirmation-mobile"> 
							{this.props.cart.map((item, i)=> { 
								
								var q=this.props.quantities[item._id] || 1;
								
								return (
					
									<div className="order-confirmation-mobile-card" key={item._id}>
										<div><strong>Name:</strong> <Link className="item-review-name" to={`/bean/${item._id}`}>{item.name}</Link><Link to={`/roaster/${item.roaster._id}`} className="item-review-roaster roaster-mobile">{item.roaster.name}</Link></div>
										<div><strong>Quantity:</strong> {q}</div>
										<div><strong>Price:</strong> {`$${item.price.toFixed(2)} each`}</div>
										<div><strong>Shipping:</strong> {this.getShippingPrice(this.state.items[i].rates, i)}</div>
										<div><strong>Total with shipping:</strong> {this.getTotalItemPrice(item, i, q)}</div>
									</div>
								
								)
							})}
							<div>
								<div className="order-confirmation-mobile-footnote">
									<p><strong>Total: <span>${this.state.total}</span></strong></p>
									<button onClick={this.placeOrder} className="checkout-button button-primary order-confirmation-button">Place order</button>
								</div>
							</div>
					</div>
				</div>
			</div>
		)
	}
}