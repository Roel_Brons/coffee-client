import React, { Component } from 'react';
import { Link } from 'react-router';
import { maxQuantity } from '../../config';

export default class ItemReview extends Component {
	
	getQuantitySelector() {
		const {quantity, updateQuantities, data} = this.props;
		let options = [];
		for (var i = 1; i <= maxQuantity; i++) {
			options.push(<option key={i} value={i}>{i}</option>);
		} 
		return (
		<select
			value={quantity}
			onChange={(e) => this.props.updateQuantities(data._id, parseInt(e.target.value))}>
			{options}
		</select>
		)
	}
	render() {
		const { _id, name, price, roaster } = this.props.data;
		return (
			<tr className="item-review">
				<td><Link className="item-review-name" to={`/bean/${_id}`}>{name}</Link><Link to={`/roaster/${roaster._id}`} className="item-review-roaster">{roaster.name}</Link></td>
				<td>{`$${price.toFixed(2)} each`}</td>
				<td>{this.getQuantitySelector()}</td>
				<td><a className="item-delete-button" onClick={() => this.props.removeFromCart(_id)}>Remove</a></td>
			</tr>
		)
	}
}