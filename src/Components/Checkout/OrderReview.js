import React, { Component } from 'react';
import { Link } from 'react-router';

export default class OrderReview extends Component {
	
	componentWillMount() {
		console.log(this.props)
	}
	render() {
		const { _id, name, price, roaster } = this.props.data; 
		return (
			<tr className="item-review">
				<td><Link className="item-review-name" to={`/bean/${_id}`}>{name}</Link><Link to={`/roaster/${roaster._id}`} className="item-review-roaster">{roaster.name}</Link></td>
				<td>{`$${price.toFixed(2)} each`}</td>
					{typeof this.props.rate != 'undefined' ? <td>${this.props.rate}</td> : null} 
				<td>{this.props.quantity}</td>
			</tr>
		)
	}
}