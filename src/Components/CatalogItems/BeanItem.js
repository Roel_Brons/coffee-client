import React, { Component } from 'react';
import { Link, hashHistory } from 'react-router';

export default class BeanItem extends Component {

	travelToPage() {
		hashHistory.push(`/bean/${this.props.data._id}`)
	}

	render() {
		const { _id, name, photoUrl, price, roaster, roast } = this.props.data;
		const { addToCart } = this.props;
		return (
			<div className="catalog-item bean-item">
				<div className="catalog-img-container">
						<img src={photoUrl} alt={name} onClick={this.travelToPage.bind(this)} />
				</div>
				<div className="catalog-info-container">
					<Link className="item-title" to={`/bean/${_id}`}>{name}</Link>
					<div className="item-roaster">{(roaster && roaster.name) || ''}</div>
					<div className="item-roast">Roast: <span>{roast}</span></div>
					<div className="catalog-info-footer">
						<div className="item-price flex-spacer">{'$'+price.toFixed(2)}</div>
						<a className="item-add-cart" onClick={() => addToCart(_id)}>Add To Cart</a>
					</div>
				</div>
			</div>
		)
	}

}