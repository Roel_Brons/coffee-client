import React, { Component } from 'react';
import { Link, hashHistory } from 'react-router';

export default class RoasterItem extends Component {

	travelToPage() {
		hashHistory.push(`/roaster/${this.props.data._id}`)
	}

	render() {
		const { _id, name, photoUrl, coffees, location } = this.props.data;
		return (
			<div className="catalog-item roaster-item">
				<div className="catalog-img-container">
					<img src={photoUrl} alt={name} onClick={this.travelToPage.bind(this)} />
				</div>
				<div className="catalog-info-container">
					<Link className="item-title" to={`/roaster/${_id}`}>{name}</Link>
					<div className="catalog-info-footer">
						<div className="item-location">{location.city}</div>
						<div className="item-coffee-count">{`${coffees.length} beans`}</div>
					</div>
				</div>
			</div>
		)
	}

}