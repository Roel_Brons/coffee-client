import Footer from './Footer/Footer.js';
import Nav from './Nav/Nav.js';
import Filter from './Filter/Filter.js';
import BeanItem from './CatalogItems/BeanItem.js';
import RoasterItem from './CatalogItems/RoasterItem.js';
import FullMap from './FullMap/FullMap.js';
import RoasterList from './RoasterList/RoasterList.js';
import CartReview from './Checkout/CartReview.js';
import CheckoutSuccess from './Checkout/CheckoutSuccess.js';
import CheckoutFail from './Checkout/CheckoutFail.js'; 
import OrderConfirmation from './Checkout/OrderConfirmation.js';

module.exports = {
	Footer,
	Nav,
	FullMap,
	Filter,
	BeanItem,
	RoasterItem,
	RoasterList,
	CartReview,
	CheckoutSuccess,
	CheckoutFail, 
	OrderConfirmation
}