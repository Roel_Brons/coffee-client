import React, {Component} from 'react';
import { Nav, Footer } from './Components';
import { hashHistory } from 'react-router';
import swal from 'sweetalert';
import '../node_modules/sweetalert/dist/sweetalert.css';

export default class Container extends Component {

	constructor(props) {
		super(props);
		this.state = {
			cart: this.getOrCreate('cart', []), 
			quantities: this.getOrCreate('quantities', {})
		}
	}

	getOrCreate(name, defaultValue) {
		let data = sessionStorage.getItem(name);
		if(data === undefined || data === null){
			sessionStorage.setItem(name, JSON.stringify(defaultValue))
			data = defaultValue;
		} else {
			data = JSON.parse(data);
		}
		if (!Array.isArray(data)) data = [];
		return data;
	}

	addToCart(beanId, beanName='Your coffee') {
		if (this.state.cart.indexOf(beanId) >= 0) {
			swal({
				title: 'Already in Cart',
				text: 'This coffee is already in your cart.',
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Checkout",
				cancelButtonText: "Continue Shopping",
			}, isConfirm => isConfirm ? hashHistory.push('cart') : '');
				return false;
			}
		const nextCart = [...this.state.cart, beanId];

		sessionStorage.setItem('cart', JSON.stringify(nextCart));
		this.setState({cart: nextCart})
		swal({
			title: 'Added to Cart',
			text: 'Awesome! '+beanName+' has been added to your cart! Would like to continue shopping or checkout?',
			type: 'success',
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Checkout",
			cancelButtonText: "Continue Shopping",
		}, isConfirm => isConfirm ? hashHistory.push('cart') : '');
	}

	removeFromCart(beanId) {
		const nextCart = this.state.cart.filter(i => i !== beanId);
		sessionStorage.setItem('cart', JSON.stringify(nextCart));
		this.setState({cart: nextCart});
	}

	emptyCart() {
		sessionStorage.setItem('cart', JSON.stringify({}));
		this.setState({cart: []});
	}

	updateQuantities(id, quantity) {
		let { quantities } = this.state;
		let temp = {};
		temp[id] = quantity;
		const nextQuantities = Object.assign({}, quantities, temp)
		sessionStorage.setItem('quantities', JSON.stringify(nextQuantities));
		this.setState({quantities: nextQuantities});
	}

	render() {
		const { cart, quantities } = this.state;

		return (
			<div className="page-container">
				<Nav cartSize= {cart.length}/>
				{React.Children.map(this.props.children, child => {
	
					return React.cloneElement(child, {
						cart,
						quantities,
						addToCart: this.addToCart.bind(this),
						removeFromCart: this.removeFromCart.bind(this),
						emptyCart: this.emptyCart.bind(this),
						updateQuantities: this.updateQuantities.bind(this)
					});
				})}
				<Footer />
			</div>
		)
	}
}