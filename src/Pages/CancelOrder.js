import React, {Component} from 'react';
import request from 'request';
import { apiEndpoint } from '../config';
import swal from 'sweetalert'; 
import { hashHistory } from 'react-router'; 

export default class CancelOrder extends Component {
	constructor(props) {
		super(props) 
		

		this.state = {
			getOrderError: false, 
			product_data: {}, 
			transaction_id: this.props.location.query.t_id, 
			quantity: this.props.location.query.q, 
			rate: this.props.location.query.rate, 
			order_id: this.props.location.query.o_id, 
			cancelFail: false, 
			cancelSuccess: false 
		} 
		
		this.cancelOrder = this.cancelOrder.bind(this); 
	}

	componentWillMount() {
		
		request({
			method: 'GET',
			uri: `${apiEndpoint}/coffee/${this.props.params.id}`
		}, (err, res, body) => {
			if(err || res.status === 400) this.setState({getOrderError: true})
			else this.setState({product_data: JSON.parse(body)})	

		})
		
	}
	cancelOrder() {
		request({
			method: 'POST', 
			uri: `${apiEndpoint}/cancel`, 
			headers: {
				'Content-Type': 'application/json'
			}, 
			body: JSON.stringify({
				t_id: this.state.transaction_id, 
				order_id: this.state.order_id, 
				bean: this.state.product_data,
				quantity: this.state.quantity,
				email: this.props.location.query.email
			})
		}, (err, res, body) => {
			
			if(err || res.statusCode === 400) {
				this.setState({cancelFail: true, cancelSuccess: false})
				swal({
					title: 'Error',
					text: 'Operation failed. Please try again',
					type: 'error',
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Close", 
					closeOnConfirm: true
				}); 
			}
			else {
				
				this.setState({cancelSuccess: true, cancelFail: false})
					swal({
						title: 'Success',
						text: 'Order has been cancelled',
						type: 'success',
						confirmButtonColor: "rgb(129, 206, 105)",
						confirmButtonText: "OK",
				}, isConfirm => isConfirm ? hashHistory.push('/') : '');
			}
		})
	}
		
	render() {
		
		return (
			<div className="cart-page page container">
				<div className="cart-review">
				<div className="cart-review-title">Order Details</div>
					<table className="cart-table">
						<thead>
							<th>Name</th><th>Quantity</th><th>Price</th><th>Shipping</th>
						</thead>
						<tbody>
							<td>{this.state.product_data.name}</td>
							<td>{this.state.quantity}</td>
							<td>${this.state.product_data.price} each</td>
							<td>${this.state.rate}</td>
						</tbody>
					</table>
				</div>
				<button className="cancel-btn" onClick={this.cancelOrder}>Cancel order</button>
				{this.state.cancelSuccess ? (
					<p className="cancel-success">Order cancelled successfully</p>
				) : null}
				{this.state.cancelFail ? (
					<p className="cancel-fail">Operation failed. Please try again.</p>
				) : null}
			</div>
		)
	}
}