import React, {Component} from 'react';
import StripeCheckout from 'react-stripe-checkout'
import request from 'request';
import { CartReview,  CheckoutFail, OrderConfirmation } from '../Components';
import { apiEndpoint, stripeKey } from '../config';

export default class Cart extends Component {

	constructor(props) {
		super(props);
		this.state = {cart: [], checkoutSuccess: false, checkoutFail: false, order: {}};
	}

	componentWillMount() {
		const beans = this.props.cart.join(',');
		if(!beans) return false;
		request(`${apiEndpoint}/coffee?beans=${beans}`, (err, res, body) => this.setState({cart: JSON.parse(body)}) );
	}

	removeFromCart(id) {
		this.setState({cart: this.state.cart.filter(i => i._id !== id)});
		this.props.removeFromCart(id);
	}

	getTotalPrice(cart=[], quantities={}) {
		const total = cart.reduce((t, i) => t + (quantities[i._id] ? quantities[i._id] * i.price : i.price), 0);
		return total.toFixed(2);
	}

	getCartForAPI() {
		const { cart, quantities } = this.props;
		//request(`${apiEndpoint}/coffee
		return cart.map(i => ({bean: i, quantity: quantities[i] || 1}));
	}

	onToken(token, address) {
		console.log(token, address);
		const newOrder = {
			email: token.email,
			shipping: {
				city : address.shipping_address_city,
				country : address.shipping_address_country,
				country_code : address.shipping_address_country_code,
				line1 : address.shipping_address_line1,
				state : address.shipping_address_state,
				zip : address.shipping_address_zip,
				name : address.shipping_name
			},
			items: this.getCartForAPI(),
			tokenID: token.id,
			charge: this.getTotalPrice(this.state.cart, this.props.quantities) * 100
		} 
		// Send Order to API
		request({
			method: 'POST',
			uri: `${apiEndpoint}/shipping`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify(newOrder),
		}, (err, res, body) => { 
			
			if(err || res.statusCode === 400) this.setState({checkoutFail: true});
			else { 
			
				body = JSON.parse(body); 
				
				for (let i = 0; i < body.items.length; i++) {
					if (body.items[i].rates.length == 0) {
						
						this.setState({checkoutFail: true});  
						return; 
					}
				} 
				
				this.setState({checkoutSuccess: true, order: body}); 
			}
		})
	}

	render() {
		const { quantities, updateQuantities, emptyCart } = this.props;
		const { checkoutSuccess, checkoutFail, cart } = this.state;
		if (checkoutSuccess) return <OrderConfirmation cart={cart} quantities={quantities} emptyCart={emptyCart} getTotalPrice={this.getTotalPrice} order={this.state.order}/>
		else if (checkoutFail) return <CheckoutFail />
		return (
			<div className="cart-page page container">
				<CartReview
					cart={cart}
					removeFromCart={this.removeFromCart.bind(this)}
					quantities={quantities}
					updateQuantities={updateQuantities} />
				<div className="cart-page-business-end">
					<div className="cart-total">Total: <span>${this.getTotalPrice(cart, quantities)}</span></div>
					<StripeCheckout
						stripeKey={stripeKey} 
						panelLabel='Proceed'
						className='checkout-button'
						token={this.onToken.bind(this)}
						shippingAddress
						name="The Worlds Coffee"
						image="http://wallpaper.sc/en/ipad/wp-content/uploads/2015/06/ipad-2048x2048-thumbnail_01469-256x256.jpg">
						<button className="checkout-button button-primary" disabled={cart.length <= 0}>Checkout</button>
					</StripeCheckout>
				</div>
				

			</div>
		)
	}
}