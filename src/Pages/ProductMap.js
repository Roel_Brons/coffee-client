import React, {Component} from 'react';
import request from 'request';
import { apiEndpoint } from '../config';
import { FullMap, RoasterList } from '../Components';

export default class ProductMap extends Component {

	constructor(props) {
		super(props);
		this.state = {roasters: [], visibleRoasters: [], highlighted: null, active: null};
	}

	componentWillMount() {
		request(`${apiEndpoint}/roaster`, (err, res, body) => this.setState({roasters: JSON.parse(body)}) )
	}

	componentDidMount() {
		this._isMounted = true;
	}

	componentWillUnmount() {
		this._isMounted = false;
	}

	updateVisibleRoasters(visibleFeatures) {
		const visibleIds = visibleFeatures.reduce((a, i) => a.concat([i.properties.id]), []);
		const visibleRoasters = this.state.roasters.filter(i => visibleIds.indexOf(i._id) >= 0);
		if(this._isMounted) this.setState({visibleRoasters: visibleRoasters});
	}

	updateHighlighted(highlighted) {
		this.setState({highlighted});
	}

	updateActive(active) {
		console.log(active);
		this.setState({active});
	}
	
	render() {
		const { roasters, visibleRoasters, highlighted, active } = this.state;
		return (
			<div className="product-map-page page">
				<RoasterList
					roasters={visibleRoasters}
					highlighted={highlighted}
					active={active}
					updateActive={this.updateActive.bind(this)}
					updateHighlighted={this.updateHighlighted.bind(this)} />
				<FullMap
					roasters={roasters}
					update={this.updateVisibleRoasters.bind(this)}
					active={active}
					query={this.props.location.query}
					updateActive={this.updateActive.bind(this)}
					highlighted={highlighted}
					updateHighlighted={this.updateHighlighted.bind(this)} />
			</div>
		)
	}
}