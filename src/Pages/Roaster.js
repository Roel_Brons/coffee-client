import React, {Component} from 'react';
import request from 'request';
import { BeanItem } from '../Components';
import { apiEndpoint } from '../config';

export default class Roaster extends Component {

	constructor(props) {
		super(props);
		this.state = {data: {}};
	}

	componentWillMount() {
		const { id } = this.props.params;
		request(`${apiEndpoint}/roaster/${id}`, (err, res, body) => this.setState({data: JSON.parse(body)}) )
	}

	render() {
		const { name, photoUrl, shortDescription, story, location, url, coffees } = this.state.data;
		const { addToCart } = this.props;
		return (
			<div className="roaster-page page container flex-center-column">
				<div className="roaster-img-container flex-center-full">
					<img src={photoUrl} alt={name}/>
				</div>
				<div className="roaster-title">{name || ''}</div>
				<div className="roaster-location">{`${(location && location.city) || ''}, ${(location && location.state) || ''}`}</div>
				<div className='roaster-content'>
					<div className="roaster-section-label">Description</div>
					<div className="roaster-description">{shortDescription || ''}</div>
					<div className="roaster-section-label">Story</div>
					<div className="roaster-story">{story || ''}</div>
					<div className="roaster-section-label">Coffee</div>
					<div className="roaster-products">
						{coffees ? coffees.map(i => <BeanItem data={i} addToCart={addToCart} />) : []}
					</div>
				</div>
			</div>
		)
	}
}