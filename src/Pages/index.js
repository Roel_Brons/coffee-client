import Home from './Home';
import About from './About';
import ProductCatalog from './ProductCatalog';
import ProductMap from './ProductMap';
import Cart from './Cart';
import Roaster from './Roaster';
import Bean from './Bean'; 
import CancelOrder from './CancelOrder'; 

module.exports = {
	Home,
	About,
	ProductMap,
	ProductCatalog,
	Cart,
	Roaster,
	Bean, 
	CancelOrder 
}