import React, {Component} from 'react';
import request from 'request';
import { hashHistory, Link } from 'react-router';
import { Filter, BeanItem, RoasterItem } from '../Components';
import { apiEndpoint } from '../config';

class SearchStatement extends Component {
	render() {
		const { searchTerm, linkWithoutSearch } = this.props;
		if(searchTerm) {
			return (
				<div className="search-statement">
					Search results for 
					<span className="search-statement-term"> {searchTerm}</span>
					<Link className="search-statement-clear" to={linkWithoutSearch}>Clear</Link>
				</div>)
		} else {
			return false;
		}
	}
}

export default class ProductCatalog extends Component {

	constructor(props) {
		super(props);
		this.state = {roasters: [], beans: []}
	}

	prevSearch: '';

	getFilters() {
		const query = this.props.location.query;
		let filters = [];
		
		if(query.subject === 'roasters'){
			if(query.location) filters = filters.concat([(data) => data.filter((i) => i.location.city === query.location)]);
		} else {
			if(query.roast) filters = filters.concat([(data) => data.filter((i) => i.roast === query.roast)]);
			if(query.priceMin) filters = filters.concat([(data) => data.filter((i) => i.price >= query.priceMin)]);
			if(query.priceMax) filters = filters.concat([(data) => data.filter((i) => i.price <= query.priceMax)]);
			if(query.location) filters = filters.concat([(data) => data.filter((i) => i.roaster.location.city === query.location)]);
		}
		return filters;
	}

	getSubject() {
		return this.props.location.query.subject || 'beans';
	}

	queryToQueryString(query) {
		let queryString = '';
		for (let i in query) {
			if (query[i] !== null && query[i] !== undefined){
				if (queryString === '') queryString += `?${i}=${query[i]}`;
				else queryString += `&${i}=${query[i]}`;
			}
		}
		return queryString;
	}

	fetchData() {
		const search = this.props.location.query.search;
		if (search && search !== '') {
			request(`${apiEndpoint}/roaster?search=${search}`, (err, res, body) => this.setState({roasters: JSON.parse(body)}) );
			request(`${apiEndpoint}/coffee?search=${search}`, (err, res, body) => this.setState({beans: JSON.parse(body)}) );
		}
		else {
			request(`${apiEndpoint}/roaster`, (err, res, body) => this.setState({roasters: JSON.parse(body)}));
			request(`${apiEndpoint}/coffee`, (err, res, body) => this.setState({beans: JSON.parse(body)}));
		}
	}

	componentDidUpdate() {
		if(this.props.location.query.search === this.prevSearch) return;
		this.prevSearch = this.props.location.query.search;
		this.fetchData();
	}

	componentWillMount() {
		this.fetchData();
	}

	updateQuery(obj) {
		const { query, pathname } = this.props.location;
		// Ensures clean filter query on subject change.
		// The second part of this tri dissallows multiple filters/queries beside subject.
		// Replace second Object.assign arg with 'query' to allow multiple filters/queries.
		const nextQuery = obj.hasOwnProperty('subject')
			? obj.hasOwnProperty('search')
				? this.queryToQueryString(obj)
				: this.queryToQueryString(Object.assign({}, obj, {search: query.search}))
			: obj.hasOwnProperty('search')
				? this.queryToQueryString(Object.assign({}, {subject: query.subject || 'beans'}, obj))
				: this.queryToQueryString(Object.assign({}, {subject: query.subject || 'beans'}, obj, {search: query.search}))
		// Triggers a rerender, allowing getCatalogItems() to reflect the new query.
		hashHistory.push(pathname + nextQuery);
	}

	getCatalogItems() {
		const { beans, roasters } = this.state;
		const { addToCart } = this.props;
		const filters = this.getFilters();
		const subject = this.getSubject();
		let items;

		if (subject === 'beans') {
			const filteredBeans = filters.reduce((o, i) => i(o), beans);
			items = filteredBeans.map(i => <BeanItem key={i._id} data={i} addToCart={addToCart}/>)
		} else {
			const filteredRoasters = filters.reduce((o, i) => i(o) , roasters);
			items = filteredRoasters.map(i => <RoasterItem key={i._id} data={i}/>)
		}
		if (items.length > 0) return <div className="catalog-items">{items}</div>
		else return <div className="no-catalog-items">Sorry, there are no {subject} that match this filter.</div>
	}

	onSearch(searchTerm) {
		if(searchTerm === '') this.updateQuery(Object.assign({}, this.props.location.query, {search: null}));
		else this.updateQuery(Object.assign({}, this.props.location.query, {search: searchTerm}));
	}

	getLinkWithoutSearch() {
		const {pathname, query} = this.props.location;
		const newQuery = Object.assign({}, query, {search:null});
		return pathname + this.queryToQueryString(newQuery);
	}

	render() {
		const searchTerm = this.props.location.query.search;
		const { roasters, beans } = this.state;
		return (
			<div className="product-catalog-page page container">
				<Filter
					roasters={roasters}
					beans={beans}
					query={this.props.location.query}
					updateQuery={this.updateQuery.bind(this)} />
				<div className="main-catalog-content">
					<SearchStatement searchTerm={searchTerm} linkWithoutSearch={this.getLinkWithoutSearch()}/>
					<div className="catalog-items">
						{this.getCatalogItems()}
					</div>
				</div>
			</div>
		)
	}
}
