import React, {Component} from 'react';
import { Link } from 'react-router';
import request from 'request';
import { apiEndpoint } from '../config';

export default class Bean extends Component {

	constructor(props) {
		super(props);
		this.state = {data: {}};
	}

	componentWillMount() {
		const { id } = this.props.params;
		request(`${apiEndpoint}/coffee/${id}`, (err, res, body) => this.setState({data: JSON.parse(body)}) )
	}

	render() {
		const { _id, name, photoUrl, description, price=0, roaster={}, roast } = this.state.data;
		const { addToCart } = this.props;
		return (
			<div className="bean-page page container flex-center-column">
				<div className="bean-page-top">
					<div className="product-img-container">
						<img src={photoUrl} alt={name}/>
					</div>
					<div className="bean-page-info">
						<div className="product-title">{name || ''}</div>
						<Link to={`roaster/${roaster._id}`} className="product-roaster">{roaster.name || ''}</Link>
						<div className="product-roast-type flex-spacer">Roast: <span>{roast || ''}</span></div>
						<div className="product-price">{'$'+price.toFixed(2) || ''}</div>
						<button className="button-primary" onClick={() => addToCart(_id)}>Add To Cart</button>
					</div>
				</div>

				<div className="product-description">{description || ''}</div>
			</div>
		)
	}
}