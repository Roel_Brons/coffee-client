import React, {Component} from 'react';
import { Link, hashHistory } from 'react-router';
import request from 'request';
import { apiEndpoint } from '../config';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

export default class Home extends Component {
	slideChanging = false

	constructor(props) {
		super(props);
		this.state = {featured: []};
	}

	componentWillMount() {
		request(`${apiEndpoint}/featured`, (err, res, body) => this.setState({featured: JSON.parse(body)}) )
	}

	getSliderContents(featured) {
		if(!featured || featured.length <= 0) return (
			<div className="slider-item">
			</div>
		);
		return featured.map(i => (
			<div className="slider-item" onClick={ () => {
				if(!this.slideChanging) window.location = i.url;
				this.slideChanging = false;
			}}>
				<a href={i.url}><img src={i.photoUrl} alt={i.name}/></a>
			</div>
		));
	}

	render() {
		const sliderContents = this.getSliderContents(this.state.featured);
		const settings = {
			dots: true,
			autoplay: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			beforeChange: () => this.slideChanging = true
		};
		return (
			<div className="home-page page container">
				<div className="welcome-message"><i>Welcome to</i><span>The </span><span>Worlds Coffee</span></div>
				<div className="slider-container">
					<Slider {...settings}>
						{sliderContents}
					</Slider>
				</div>
				<div className="action-set">
					<Link className="button" to="catalog/?subject=beans">Browse Coffees</Link>
					<Link className="button" to="catalog/?subject=roasters">Browse Roasters</Link>
					<Link className="button" to="map">Explore Our Roaster Map</Link>
				</div>
			</div>
		)
	}
}
