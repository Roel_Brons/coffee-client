import React, {Component} from 'react';
import request from 'request';
import { apiEndpoint } from '../config';

export default class Home extends Component {

	render() {
		return (
			<div className="about-page page container">
				<video id="about-video" src="https://dl.dropboxusercontent.com/s/fg6s76yszsi9ppi/video-1481902514%20%282%29.mp4?dl=0" autoPlay></video>
				<p className="about-text">Life is more enjoyable, more fulfilling, when you try new things. When I started on my idea to start The World’s Coffee I wanted to own my own coffee shop. Yet mixing with a love for traveling and trying out every coffee shop along the way, is where I discovered the vastness of coffee. With deep flavor characteristics due to the type of the bean, roasting temperature, age of the bean and other characteristics all of which derives a roaster’s secret recipe. This is the gift of knowledge and experience that I learned from my travels which is what I wish to give back to the world. Creating a closer world from roaster to customer. </p>
				<p className="about-sign-off">We look forward to seeing you soon! <span>-Kolin</span></p>

				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />

				<a name="privacy"></a>
				<p>THE WORLDS COFFEE PRIVACY STATEMENT</p>

				<p>Last Revised: 12 January 2017</p>

				<p>
				    This Privacy Statement ("Statement") applies to the website located at theworldscoffee.com, The Worlds Coffee (US market), and any other websites owned and operated by The Worlds Coffee that direct the viewer or user to this Statement. In this Statement, the terms "The Worlds Coffee," "we," and "us" refers to The Worlds Coffee Corporation and its respective subsidiaries and affiliated companies. Websites that are owned and operated by The Worlds Coffee may contain links to websites that are owned and operated by other companies. This Statement does not apply to websites and services that are owned and operated by third parties.
				</p>

				<p>Information We Collect</p>

				<p>
				    As you use our services, we collect certain information about you and the services you use. The information we collect falls into three different categories: (1) information you give us; (2) information we collect from you automatically when you use our services; and (3) information we collect from other sources.
				</p>

				<p>Information You Give Us</p>

				<p>
				    We collect information you give us when you use our services. Some examples of using our "services" are when you visit one of our stores, visit one of our websites, create a The Worlds Coffee account, buy a gift card online, or participate in a survey or promotion. The information you give us may include your first or last name, username, password, email address, postal address, phone number, financial account information such as a credit card number, birthday, city of birth, demographics information, and any other information you choose to give us.
				</p>

				<p>Information We Collect When You Use Our Services</p>

				<p>
				    When you use our services, we may collect information about the services you use and how you use them. This information includes: <br />
				    Purchasing Information – We may collect information about the products you buy, including where you buy products, how frequently you buy products, and the rewards earned due to a purchase;<br />
				    Device and Website Use Information – When you install one of our mobile applications or use a computer, tablet, smart phone or other device to access our websites or purchase products or services via our online properties, we may collect information about the device and how you use it.<br />
				    This information may include the type of device, your operating system, your browser (for example, whether you used Internet Explorer, Firefox, Safari, Chrome or another browser), your internet service provider, your domain name, your internet protocol (IP) address, your device identifier (or UDID), the date and time that you accessed our service, the website that referred you to our website, the web pages you requested, the date and time of those requests, and the subject of the ads you click or scroll over. To collect this information, we use cookies, web beacons and similar technologies.
				</p>

				<p>
				    Location Information – When you use a smart phone or other mobile device to access our services, use a computer to access our website or use our mobile application, we may collect information about your physical location.<br />
				    We may combine this information with other location-based information, such as your IP address and billing or postal code, to give you information about stores near you and to provide you with other services on your mobile device. We share your location information only with companies that help us provide you with services and help us understand how the services are being used.
				</p>

				<p>Information We Collect From Other Sources</p>
				<p>
				    We also collect information that is publicly available. For example, we may collect information you submit to a blog, a chat room, or a social network like Facebook, Twitter or Google+. We may also collect information about you from other companies and organizations. By gathering additional information about you, we can correct inaccurate information and give you product recommendations and special offers that are more likely to interest you.
				</p>

				<p>How We Use the Information We Collect</p>
				<p>
				    We may use the information we collect about you in a variety of ways, including:<br />
				    to process your purchases of products and services;<br />
				    to communicate with you about orders, purchases, services, accounts, programs, contests, and sweepstakes;<br />
				    to respond to your customer service inquiries and requests for information;<br />
				    to post your comments or statements on our websites;<br />
				    to send you personalized promotions and special offers;<br />
				    to inform you about our brands, products, events, or other promotional purposes;<br />
				    to maintain and improve our sites and the products and services we offer;<br />
				    to detect, prevent, or investigate security breaches or fraud; and<br />
				    to maintain appropriate records for internal administrative purposes.
				</p>

				<p>How We Share the Information We Collect</p>
				<p>
				    We may share your information in the following circumstances:<br />
				    When We Work Together – We may share the collected information between The Worlds Coffeeand, its subsidiaries, and affiliated companies for proper management and analysis and decision making, including decisions regarding the expansion and promotion of our product and service offering, order or customer request fulfillment, and for use by those companies for the other purposes described in this Statement.<br />
				    When We Work with Service Providers – We may share your information with service providers that provide us with support services, such as credit card processing, website hosting, email delivery, location mapping, postal mail processing and delivery, and analytics services. We require the companies to refrain from collecting, using and disclosing your information except when they are performing work for us or when the disclosure of your information is required by law.<br />
				    When We Work on Business Transactions – If we become involved with a merger or another situation involving the transfer of some or all of our business assets, we may share your information with business entities or people involved in the negotiation or transfer.<br />
				    When Sharing Helps Us Protect Lawful Interests – We may disclose your information if we believe that the disclosure is required by law, if we believe that the disclosure is necessary to enforce our agreements or policies, or if we believe that the disclosure will help us protect the rights, property, or safety of TheWorlds Coffee or our customers or partners.<br />
				    When We Share with Other Companies for Marketing Purposes – We may share your information with service providers to use your information to provide you with promotions and special offers. We also may share anonymous, aggregated statistics about your use of our websites and services with other companies.<br />
				    When You Give Consent – We may share information about you with other companies if you give us permission or direct us to share the information.<br />
				    When the Information Does Not Identify You – We may share your information in a way that does not directly identify you. For example, we may combine information about you with information about other people and share the combined information in a way that does not link your information to you.<br />
				    When You Post on Our Websites – If you post information on a blog or another part of our websites, the information that you post may be seen by other visitors to our websites, including your post information and your user name.
				</p>

				<p>How We May Allow Others to Collect Your Information</p>
				<p>
				    When you use our websites or other services, we may allow third parties to collect information about you by setting their own cookies, web beacons and other similar technologies on our website. The information collected by third parties may include the type of device, your operating system, your browser, your internet service provider, your domain name, your internet protocol (IP) address, the date and time that you accessed our service, the website that referred you to our website, the web pages you requested, the date and time of those requests, and the subject of the ads you click or scroll over.<br />
				    We allow third parties to collect this information on our websites for the following purposes:<br />
				    To Display Ads for Products or Services – We allow some advertising companies to collect information in order to display ads on our websites that are most relevant to you. These third parties may also collect anonymous information about visitors to our websites to place them into market segments to display ads on other websites believed to be relevant to each market segment. This is known as "online behavioral advertising." Please see below for more information about opting out of these targeted display ads and controlling the use of cookies, web beacons, and similar technologies.<br />
				    To Collect Information on How Our Websites and Services Are Used – We allow certain service providers to use the information collected on our websites and services to help us learn about our audience and how people use our websites. The companies that use this information for this purpose do not match the information to individual users. In other words, statistical information collected by third parties regarding website usage or performance is not matched or linked to you.<br />
				    To Make the Services of Other Companies Work on Our Websites – We allow companies to use special technologies to make certain parts of our websites work. For example, we allow Adobe to set and enable special cookies that are necessary to deliver video content for Adobe Flash Player. These cookies are called Flash cookies.<br />
				    To Link Your Activity on Our Websites to Your Social Networks – We have added certain features to our websites that allow social networks (such as Facebook, Twitter, and Google+) to track the activities of their members or collect certain information about website visitors. These features may include technologies called "plug-ins" and "widgets." Plug-ins create a direct link between two websites, and widgets are interactive mini-programs that provide other companies’ services on our websites. If you are concerned about the way a social network is tracking your activity, please contact the social network or review its privacy policy. For example, you can review Facebook’s data-use policy at https://www.facebook.com/about/privacy/.
				</p>

				<p>Cookies, Web Beacons and Similar Technologies</p>
				<p>
				    We and others may use a variety of technologies to collect information about your device and use of our services. These technologies include cookies and web beacons:<br />
				    Cookies – Cookies are small data files that are sent from a website’s server and are stored on your device’s hard drive either for only the duration of your visit ("session cookies") or for a fixed period of time ("persistent cookies"). Cookies contain information that can later be read by a web server. Most web browsers automatically accept cookies, but you can change your browser settings to give you the choice to accept a cookie or reject cookies altogether. For more information about changing your browser settings, please see the section of this Statement titled, "Your Choices ."<br />
				    We mainly use cookies for the following purposes:<br />
				    Website Functionality – Some cookies (considered "strictly necessary") are required to allow you to access and use our websites. Without these cookies, our websites do not work properly.<br />
				    Performance Monitoring – Some cookies (considered "performance" cookies) help us analyze and estimate traffic on our website. They show us how visitors interact with our website, whether there are any errors, which pages are not used often, which pages take a long time to load, which pages users tend to visit and in what order. These cookies do not collect any information that could identify you and are only used to help us improve how our website works and understand user interests.<br />
				    User Convenience – Some cookies (considered "functionality" cookies) remember information to save you the trouble of entering information every time you visit or use a particular website. For example, a cookie may remember your username to save you time when you log in to your account.<br />
				    Marketing – Some cookies (considered "targeting or advertising" cookies) are used to tailor your experience on our website by controlling the promotions, advertisements and other marketing messages that may appear when you visit or use our website and help us learn which services you are using and how you are accessing information about us. We may use this information to personalize your visit to a website or to send you relevant promotions.<br />
				    Web Beacons – Web beacons are small, transparent images that are embedded in web pages, applications, and emails that are sometimes called "clear gifs," "single pixel gifs", "page tags" or "web bugs." We use web beacons to track the web pages you visit, to test the effectiveness of our marketing, and to find out if an email has been opened and acted on.
				</p>

				<p>Information Collection Choice</p>
				<p>
				    You can also make choices about the information we collect about you:<br />
				    Location Information – When you use a smart phone or other mobile device to access our websites, we may collect information about your physical location only if (a) "location services" for the mobile application is enabled; or (b) the permissions in the mobile device allow communication of this information. If you do not want us to collect your location information, you can opt out of sharing this information by changing the relevant preferences and permissions in your mobile device.<br />
				    Cookies – If you want to reject cookies, you must take action to select the appropriate settings in your browser. Each browser’s cookie control is a little different, but the most common browsers (Internet Explorer, Chrome, Firefox, and Safari) have a preference or option you can select so the browser notifies you if a site wants to set a cookie and typically provides an option to accept or reject the cookie before the cookie is set. If you choose to remove or reject cookies, it will affect many features or services on our websites. If you want to learn the correct way to modify your cookie settings, please use the Help menu in your browser. For additional information about cookies, including how to refuse cookies, please visit: www.allaboutcookies.org .
				    In addition to adjusting the appropriate settings in your browser, many advertising companies that may collect anonymous information for advertising targeting purposes are also members of the Digital Advertising Alliance or the Network Advertising Initiative, both of which provide an opt-out of advertisement targeting from their members located at their respective websites at www.AboutAds.info and www.networkadvertising.org .<br />
				    Flash Cookies – We allow Adobe to set and enable special cookies that are necessary to deliver video content for Adobe Flash Player. You cannot remove Flash cookies simply by changing your browser settings. If you would like to limit the websites that can store information in Flash cookies on your device, you must visit the Adobe website: http://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager07.html .<br />
				    "Do Not Track" Technology – Some newer web browsers have a "Do Not Track" preference that transmits a "Do Not Track" header to the websites you visit with information indicating that you do not want your activity to be tracked. We currently do not respond to browser "Do Not Track" signals.
				</p>

				<p>Changing Your Information or Deleting Your Account</p>
				<p>
				    If you want to change your information, cancel your account, or stop us from using your information, please contact us as described in the "Contact Us" section, below. We will make every effort to promptly respond to your request. We will retain your information for as long as your account is active or as needed to provide you services. If you ask us to delete your account, we generally retain and use your account information only as long as necessary to fulfill a business or law enforcement need.
				</p>

				<p>How We Protect Your Information</p>
				<p>
				    TheWorlds Coffee protects your information using technical, physical, and administrative security measures to reduce the risk of loss, misuse, unauthorized access, disclosure or modification of your information. Some of our safeguards include firewalls, data encryption, physical access controls, and administrative informational controls. When you transmit highly sensitive information (such as a credit card number) through our website or in one of our mobile applications, we encrypt the transmission of that information using the Secure Sockets Layer (SSL) protocol. While we have employed security technologies and procedures to assist safeguarding your personal information, no system or network can be guaranteed to be 100% secure.
				</p>


				<p>Use by Minors</p>
				<p>
				    We do not intend for our websites or online services to be used by anyone under the age of 13. If you are a parent or guardian and believe we may have collected information about a child, please contact us as described in the "Contact Us" section, below.
				</p>

				<p>Changes to This Privacy Statement</p>
				<p>
				    This Statement went into effect on the date noted at the top of this webpage. We may update this Statement from time to time. If we make material changes, we will post the updated Statement on this page and change the date at the top of this webpage. We encourage you to look for updates and changes to this Statement by checking this date at the top of this webpage. We will notify you of any modifications to this Statement that might materially affect the way we use or disclose your personal information prior to the change becoming effective by means of a message on this website.
				</p>


				<p>Contact Us</p>
				<p>
				    We welcome your questions, comments and concerns about privacy. You can contact The Worlds Coffee online at www.theworldscoffee@protonmail.com
				</p>

				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />

				<a name="terms"></a>

				<p>Terms and Conditions</p>

				<p>These terms and conditions outline the rules and regulations for the use of The Worlds Coffees Website. </p>

				<p>
				    The Worlds Coffee is located at:<br />
				    Austin, Texas 78258<br />
				    United States
				</p>

				<p>
				    By accessing this website we assume you accept these terms and conditions in full. Do not continue to use The Worlds Coffees website if you do not accept all of the terms and conditions stated on this page.
				</p>

				<p>
				    The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and any or all Agreements: "Client", “You” and “Your” refers to you, the person accessing this website and accepting the Company’s terms and conditions. "The Company", “Ourselves”, “We”, “Our” and "Us", refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves, or either the Client or ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner, whether by formal meetings of a fixed duration, or any other means, for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services/products, in accordance with and subject to, prevailing law of United States. Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to same.
				</p>

				<p>Cookies</p>

				<p>
				    We employ the use of cookies. By using The Worlds Coffees website you consent to the use of cookies in accordance with The Worlds Coffee’s privacy policy.<br />

				    Most of the modern day interactive web sites use cookies to enable us to retrieve user details for each visit. Cookies are used in some areas of our site to enable the functionality of this area and ease of use for those people visiting. Some of our affiliate / advertising partners may also use cookies.
				</p>

				<p>License</p>

				<p>
				    Unless otherwise stated, The Worlds Coffee and/or it’s licensors own the intellectual property rights for all material on The Worlds Coffee All intellectual property rights are reserved. You may view and/or print pages from http://theworldscoffee.com for your own personal use subject to restrictions set in these terms and conditions.<br />

				    You must not:<br />

				    Republish material from http://theworldscoffee.com<br />
				    Sell, rent or sub-license material from http://theworldscoffee.com<br />
				    Reproduce, duplicate or copy material from http://theworldscoffee.com<br />
				    Redistribute content from The Worlds Coffee (unless content is specifically made for redistribution).
				</p>

				<p>User Comments</p>

				<p>
				    This Agreement shall begin on the date hereof.<br />
				    Certain parts of this website offer the opportunity for users to post and exchange opinions, information, material and data ('Comments') in areas of the website. The Worlds Coffee does not screen, edit, publish or review Comments prior to their appearance on the website and Comments do not reflect the views or opinions of The Worlds Coffee, its agents or affiliates. Comments reflect the view and opinion of the person who posts such view or opinion. To the extent permitted by applicable laws The Worlds Coffee shall not be responsible or liable for the Comments or for any loss cost, liability, damages or expenses caused and or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.
				    The Worlds Coffee reserves the right to monitor all Comments and to remove any Comments which it considers in its absolute discretion to be inappropriate, offensive or otherwise in breach of these Terms and Conditions.<br />
				    You warrant and represent that:<br />
				    You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;<br />
				    The Comments do not infringe any intellectual property right, including without limitation copyright, patent or trademark, or other proprietary right of any third party;<br />
				    The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material or material which is an invasion of privacy.<br />
				    The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.
				    You hereby grant to The Worlds Coffee a non-exclusive royalty-free license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.
				</p>

				<p>Hyperlinking to our Content</p>

				<p>
				    The following organizations may link to our Web site without prior written approval:<br />
				    Government agencies;<br />
				    Search engines;<br />
				    News organizations;<br />
				    Online directory distributors when they list us in the directory may link to our Web site in the same manner as they hyperlink to the Web sites of other listed businesses; and<br />
				    Systemwide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.<br />
				    These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party's site.<br />
				    We may consider and approve in our sole discretion other link requests from the following types of organizations:<br />
				    commonly-known consumer and/or business information sources such as Chambers of Commerce, American Automobile Association, AARP and Consumers Union;<br />
				    dot.com community sites;<br />
				    associations or other groups representing charities, including charity giving sites;<br />
				    online directory distributors;<br />
				    internet portals;<br />
				    accounting, law and consulting firms whose primary clients are businesses; and<br />
				    educational institutions and trade associations.<br />
				    We will approve link requests from these organizations if we determine that: (a) the link would not reflect unfavorably on us or our accredited businesses (for example, trade associations or other organizations representing inherently suspect types of business, such as work-at-home opportunities, shall not be allowed to link); (b)the organization does not have an unsatisfactory record with us; (c) the benefit to us from the visibility associated with the hyperlink outweighs the absence of The Worlds Coffee; and (d) where the link is in the context of general resource information or is otherwise consistent with editorial content in a newsletter or similar product furthering the mission of the organization.
				</p>

				<p>
				    These organizations may link to our home page, to publications or to other Web site information so long as the link: (a) is not in any way misleading; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and it products or services; and (c) fits within the context of the linking party's site.
				</p>

				<p>
				    If you are among the organizations listed in paragraph 2 above and are interested in linking to our website, you must notify us by sending an e-mail to theworldscoffee@protonmail.com. Please include your name, your organization name, contact information (such as a phone number and/or e-mail address) as well as the URL of your site, a list of any URLs from which you intend to link to our Web site, and a list of the URL(s) on our site to which you would like to link. Allow 2-3 weeks for a response.
				</p>

				<p>
				    Approved organizations may hyperlink to our Web site as follows:<br />
				    By use of our corporate name; or<br />
				    By use of the uniform resource locator (Web address) being linked to; or<br />
				    By use of any other description of our Web site or material being linked to that makes sense within the context and format of content on the linking party's site.<br />
				    No use of (cname)’s logo or other artwork will be allowed for linking absent a trademark license agreement.
				</p>

				<p>Iframes</p>

				<p>
				    Without prior approval and express written permission, you may not create frames around our Web pages or use other techniques that alter in any way the visual presentation or appearance of our Web site.
				</p>

				<p>Content Liability</p>

				<p>
				    We shall have no responsibility or liability for any content appearing on your Web site. You agree to indemnify and defend us against all claims arising out of or based upon your Website. No link(s) may appear on any page on your Web site or within any context containing content or materials that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.
				</p>

				<p>Reservation of Rights</p>

				<p>
				    We reserve the right at any time and in its sole discretion to request that you remove all links or any particular link to our Web site. You agree to immediately remove all links to our Web site upon such request. We also reserve the right to amend these terms and conditions and its linking policy at any time. By continuing to link to our Web site, you agree to be bound to and abide by these linking terms and conditions.
				</p>

				<p>Removal of links from our website</p>

				<p>
				    If you find any link on our Web site or any linked web site objectionable for any reason, you may contact us about this. We will consider requests to remove links but will have no obligation to do so or to respond directly to you.<br />
				    Whilst we endeavour to ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we commit to ensuring that the website remains available or that the material on the website is kept up to date.
				</p>

				<p>Disclaimer</p>

				<p>
				    To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website (including, without limitation, any warranties implied by law in respect of satisfactory quality, fitness for purpose and/or the use of reasonable care and skill). Nothing in this disclaimer will:<br />
				    limit or exclude our or your liability for death or personal injury resulting from negligence;<br />
				    limit or exclude our or your liability for fraud or fraudulent misrepresentation;<br />
				    limit any of our or your liabilities in any way that is not permitted under applicable law; or<br />
				    exclude any of our or your liabilities that may not be excluded under applicable law.<br />
				    The limitations and exclusions of liability set out in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer or in relation to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.<br />
				    To the extent that the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.
				</p>

			</div>
		)
	}

}
