import { mapboxKey, mapboxStyle } from './config';
const MapboxGeocoding = require('mapbox/lib/services/geocoding');
const client = new MapboxGeocoding(mapboxKey);

const mapbox = require('mapbox-gl/dist/mapbox-gl.js');
mapbox.accessToken = mapboxKey;

export const initializeMap = (container, lat=30.2672, lng=-97.7431, zoom=3) => {
	// Initializes a map focused over Austin , TX
	return new mapbox.Map({
		container,
		style: mapboxStyle,
		center: [lng, lat],
		zoom: zoom,
	})
}

export const geoJSON = (items, getCoordinates) => {
	let geo = {
		type: "FeatureCollection"
	}
	geo.features = items.map(i => ({
		type: "Feature",
		geometry: {
			type: "Point",
			coordinates: getCoordinates(i),
		},
		properties: {
			id: i._id,
			hover: 0,
		}
	}))
	return geo;
}

export const getCityState = (lat, lng, cb) => {
	client.geocodeReverse({latitude: lat, longitude: lng}, {types: 'place,region'}, (err, res) => {
		if(err) return cb(err);
		if(res.features.length <= 0) return false;
		// Takes the response's feature set and transforms it into an object like {city: 'Austin', state: 'Texas'}
		const data = res.features.map(f => f.id.startsWith('place') ? {city: f.text} : {state: f.text}).reduce((o1, o2) => Object.assign(o1, o2), {});
		cb(undefined, data);
	});
}

export const getStaticMap = (lat, lng) => {
	return `https://api.mapbox.com/v4/mapbox.streets/pin-m-cafe+FF5656(${lng},${lat})/${lng},${lat},15/200x200.png?access_token=${mapboxKey}`
}
