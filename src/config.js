export const apiEndpoint = 'http://localhost:5000';
export const mapboxKey = 'pk.eyJ1Ijoibmlja3JvYmVydHM0MDQiLCJhIjoiMC1kS1RXdyJ9.lbCLAKKF-uTLu6Hx-0Ppbg';
export const mapboxStyle = 'mapbox://styles/mapbox/light-v9'; 
export const stripeKey = 'pk_test_sJldqpdwxC7EZApV0rtey1M7';
//export const stripeKey = 'pk_live_k6Mtna3KNcH1Bn08IQ1L973A';
export const footerText = 'Copyright 2017';
export const maxQuantity = 5;
