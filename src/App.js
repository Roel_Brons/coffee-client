import React, { Component } from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import { Home, About, ProductMap, ProductCatalog, Roaster, Bean, Cart, PrivacyPolicy, TermsOfService, CancelOrder} from './Pages'
import Container from './Container';

export default class App extends Component {

	render() {
		return (
			<Router history={hashHistory}>
				<Route path="/" component={Container}>
					<IndexRoute component={Home} />
					<Route path="about" component={About} />
					<Route path="map" component={ProductMap} />
					<Route path="catalog" component={ProductCatalog} />
					<Route path="cart" component={Cart} />
					<Route path="roaster/:id" component={Roaster} />
					<Route path="bean/:id" component={Bean} />
					<Route path="privacy" component={PrivacyPolicy} />
					<Route path="terms" component={TermsOfService} />
					<Route path="cancel/:id" component={CancelOrder} />
				</Route>
			</Router>
		);
	}
}
